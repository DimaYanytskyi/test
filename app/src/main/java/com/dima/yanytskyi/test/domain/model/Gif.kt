package com.dima.yanytskyi.test.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Gif(
    val originalUrl: String,
    var deleted: Boolean,
    @PrimaryKey val id: Int? = null
)