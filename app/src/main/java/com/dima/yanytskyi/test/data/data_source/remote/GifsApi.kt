package com.dima.yanytskyi.test.data.data_source.remote

import com.dima.yanytskyi.test.common.Constants.API_KEY
import com.dima.yanytskyi.test.data.data_source.remote.dto.GifsDto
import retrofit2.http.GET
import retrofit2.http.Query

interface GifsApi {

    @GET("v1/gifs/search")
    suspend fun fetchGifsByQuery(
        @Query("api_key") api_key: String = API_KEY,
        @Query("q") q: String,
        @Query("limit") limit: Int = 25,
        @Query("offset") offset: Int,
        @Query("rating") rating: String = "g",
        @Query("lang") lang: String = "en"
    ) : GifsDto
}