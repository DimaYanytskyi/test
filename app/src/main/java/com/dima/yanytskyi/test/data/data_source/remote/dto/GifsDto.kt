package com.dima.yanytskyi.test.data.data_source.remote.dto

data class GifsDto(
    val data: List<Data>,
    val meta: Meta,
    val pagination: Pagination
)