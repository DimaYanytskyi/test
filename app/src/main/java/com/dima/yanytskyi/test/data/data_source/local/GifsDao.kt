package com.dima.yanytskyi.test.data.data_source.local

import androidx.room.*
import com.dima.yanytskyi.test.domain.model.Gif

@Dao
interface GifsDao {

    @Query("SELECT * FROM gif")
    suspend fun getGifs() : List<Gif>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGifsList(gifs: List<Gif>)
}