package com.dima.yanytskyi.test.presentation.main_screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.dima.yanytskyi.test.R
import com.dima.yanytskyi.test.domain.model.Gif

class MainAdapter(
    private val gifs: List<Gif>,
    private val onClick: (List<Gif>) -> Unit
) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    inner class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val gif: ImageView = itemView.findViewById(R.id.gif)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.main_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        Glide
            .with(holder.itemView.context)
            .load(gifs[position].originalUrl)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(holder.gif)
        holder.itemView.setOnClickListener { onClick(gifs.subList(position, gifs.size)) }
    }

    override fun getItemCount(): Int = gifs.size
}