package com.dima.yanytskyi.test.domain.repository

import com.dima.yanytskyi.test.data.data_source.remote.dto.GifsDto
import com.dima.yanytskyi.test.domain.model.Gif

interface GifsRepository {
    suspend fun fetchGifs(q: String, offset: Int) : GifsDto
    suspend fun getGifs() : List<Gif>
    suspend fun insertGifs(list: List<Gif>)
}