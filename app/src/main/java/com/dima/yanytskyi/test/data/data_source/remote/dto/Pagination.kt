package com.dima.yanytskyi.test.data.data_source.remote.dto

data class Pagination(
    val count: Int,
    val offset: Int,
    val total_count: Int
)