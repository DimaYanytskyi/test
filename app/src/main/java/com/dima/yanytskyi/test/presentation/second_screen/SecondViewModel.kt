package com.dima.yanytskyi.test.presentation.second_screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide.init
import com.dima.yanytskyi.test.domain.interactors.GifsInteractor
import com.dima.yanytskyi.test.domain.model.Gif
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SecondViewModel @Inject constructor(
    private val gifsInteractor: GifsInteractor
) : ViewModel(){

}