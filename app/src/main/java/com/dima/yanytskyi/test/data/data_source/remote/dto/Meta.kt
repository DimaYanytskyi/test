package com.dima.yanytskyi.test.data.data_source.remote.dto

data class Meta(
    val msg: String,
    val response_id: String,
    val status: Int
)