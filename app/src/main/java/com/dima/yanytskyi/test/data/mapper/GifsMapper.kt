package com.dima.yanytskyi.test.data.mapper

import com.dima.yanytskyi.test.data.data_source.remote.dto.GifsDto
import com.dima.yanytskyi.test.domain.model.Gif

fun GifsDto.toGifList() : List<Gif> {
    return data.map {
        Gif(
            it.images.original.url,
            false
        )
    }
}