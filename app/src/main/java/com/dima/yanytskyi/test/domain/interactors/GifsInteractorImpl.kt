package com.dima.yanytskyi.test.domain.interactors

import com.dima.yanytskyi.test.data.mapper.toGifList
import com.dima.yanytskyi.test.domain.model.Gif
import com.dima.yanytskyi.test.domain.repository.GifsRepository
import com.sobe.favorit.sporter.common.Resources
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException
import javax.inject.Inject

class GifsInteractorImpl @Inject constructor(
    private val repository: GifsRepository
) : GifsInteractor {

    override fun fetchGifs(q: String, offset: Int): Flow<Resources<List<Gif>>> = flow {
        try {
            emit(Resources.Loading())
            val gifs = repository.fetchGifs(q, offset).toGifList()
            emit(Resources.Success(gifs))
        } catch(e: IOException) {
            emit(Resources.Error(e.message ?: e.toString()))
        }
    }

    override fun getGifs(): Flow<Resources<List<Gif>>> = flow {
        try {
            emit(Resources.Loading())
            val gifs = repository.getGifs()
            emit(Resources.Success(gifs))
        } catch (e: IOException) {
            emit(Resources.Error(e.message ?: e.toString()))
        }
    }

    override suspend fun insertGifs(list: List<Gif>) {
        repository.insertGifs(list)
    }

}