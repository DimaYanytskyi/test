package com.dima.yanytskyi.test.presentation.main_screen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dima.yanytskyi.test.R
import com.dima.yanytskyi.test.databinding.FragmentMainBinding
import com.dima.yanytskyi.test.domain.model.Gif
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {

    private lateinit var binding: FragmentMainBinding
    private val viewModel : MainViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)

        binding.queryInput.addTextChangedListener {
            viewModel.onEvent(MainScreenEvent.OnSearchInputChanged(it.toString()))
        }

        binding.loadMore.setOnClickListener {
            viewModel.onEvent(MainScreenEvent.OnNext)
        }

        binding.previousPage.setOnClickListener {
            viewModel.onEvent(MainScreenEvent.OnPrev)
        }

        viewModel.loading.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = if(it) View.VISIBLE else View.GONE
        }

        viewModel.error.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }

        viewModel.gifs.observe(viewLifecycleOwner) { gifs ->
            val mainAdapter = MainAdapter(gifs) {
                findNavController()
                    .navigate(
                        R.id.action_mainFragment_to_secondFragment,
                        bundleOf("gifsList" to it.map { it.originalUrl }.toTypedArray())
                    )
            }
            binding.gifsRecycler.apply {
                isNestedScrollingEnabled = false
                layoutManager = GridLayoutManager(requireContext(), 2)
                adapter = mainAdapter
            }
        }
    }
}