package com.dima.yanytskyi.test.data.data_source.remote.dto

data class FixedWidthDownsampled(
    val height: String,
    val size: String,
    val url: String,
    val webp: String,
    val webp_size: String,
    val width: String
)