package com.dima.yanytskyi.test.data.data_source.remote.dto

data class DownsizedMedium(
    val height: String,
    val size: String,
    val url: String,
    val width: String
)