package com.dima.yanytskyi.test.presentation.second_screen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.dima.yanytskyi.test.R
import com.dima.yanytskyi.test.databinding.FragmentSecondBinding
import com.dima.yanytskyi.test.domain.model.Gif
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SecondFragment : Fragment(R.layout.fragment_second) {

    private lateinit var binding: FragmentSecondBinding
    private val viewModel : SecondViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSecondBinding.bind(view)

        val gifsList = requireArguments().getStringArray("gifsList")

        binding.back.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.fullscreenView.apply {
            adapter = FullscreenGifAdapter(gifsList?.map { Gif(it, false) } ?: emptyList())
            currentItem = 1
        }
    }
}