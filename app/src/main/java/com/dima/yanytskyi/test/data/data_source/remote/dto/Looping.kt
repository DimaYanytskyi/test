package com.dima.yanytskyi.test.data.data_source.remote.dto

data class Looping(
    val mp4: String,
    val mp4_size: String
)