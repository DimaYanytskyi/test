package com.dima.yanytskyi.test.di

import android.app.Application
import androidx.room.Room
import com.dima.yanytskyi.test.common.Constants
import com.dima.yanytskyi.test.common.Constants.DATABASE_NAME
import com.dima.yanytskyi.test.data.data_source.local.GifsDao
import com.dima.yanytskyi.test.data.data_source.local.GifsDatabase
import com.dima.yanytskyi.test.data.data_source.remote.GifsApi
import com.dima.yanytskyi.test.data.repository.GifsRepositoryImpl
import com.dima.yanytskyi.test.domain.interactors.GifsInteractor
import com.dima.yanytskyi.test.domain.interactors.GifsInteractorImpl
import com.dima.yanytskyi.test.domain.repository.GifsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideNoteDatabase(app: Application) : GifsDao{
        return Room.databaseBuilder(
            app,
            GifsDatabase::class.java,
            DATABASE_NAME
        ).build().gifsDao
    }

    @Provides
    @Singleton
    fun provideGifsApi() : GifsApi {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GifsApi::class.java)
    }

    @Provides
    @Singleton
    fun provideGifsRepository(
        gifsApi: GifsApi,
        gifsDao: GifsDao
    ) : GifsRepository {
        return GifsRepositoryImpl(gifsApi, gifsDao)
    }

    @Provides
    @Singleton
    fun provideGifsInteractor(gifsRepository: GifsRepository) : GifsInteractor {
        return GifsInteractorImpl(gifsRepository)
    }
}