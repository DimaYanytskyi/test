package com.dima.yanytskyi.test.presentation.main_screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dima.yanytskyi.test.domain.interactors.GifsInteractor
import com.dima.yanytskyi.test.domain.model.Gif
import com.sobe.favorit.sporter.common.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val gifsInteractor: GifsInteractor
) : ViewModel() {

    private val _gifs = MutableLiveData<List<Gif>>(emptyList())
    val gifs: LiveData<List<Gif>> = _gifs

    private val _localGifs = MutableLiveData<List<Gif>>(emptyList())

    private val _offset = MutableLiveData(0)

    private val _q = MutableLiveData("loading")

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private var fetchJob: Job? = null

    init {
        getGifs()
        fetchGifs()
    }

    fun onEvent(event: MainScreenEvent){
        when(event){
            is MainScreenEvent.OnNext -> {
                _offset.value = _offset.value?.plus(25)
                fetchGifs()
            }
            is MainScreenEvent.OnPrev -> {
                if(_offset.value!! >= 25){
                    _offset.value = _offset.value?.minus(25)
                    fetchGifs()
                } else {
                    _error.value = "Can't find page"
                }
            }
            is MainScreenEvent.OnSearchInputChanged -> {
                _q.value = if(event.text.isEmpty()) "loading" else event.text
                fetchGifs()
            }
        }
    }

    private fun getGifs() {
        viewModelScope.launch {
            gifsInteractor.getGifs().collect {
                when(it){
                    is Resources.Loading -> {
                        _loading.value = true
                    }
                    is Resources.Success -> {
                        _loading.value = false
                        _localGifs.value = it.data ?: emptyList()
                    }
                    is Resources.Error -> {
                        _loading.value = false
                        _error.value = it.message ?: "An unexpected error occurred"
                    }
                }
            }
        }
    }

    private fun fetchGifs(){
        fetchJob?.cancel()
        fetchJob = viewModelScope.launch {
            gifsInteractor.fetchGifs(
                _q.value!!,
                _offset.value!!
            ).collectLatest {
                when(it){
                    is Resources.Loading -> {
                        _loading.value = true
                    }
                    is Resources.Success -> {
                        _loading.value = false
                        _gifs.value = it.data ?: emptyList()
                    }
                    is Resources.Error -> {
                        _loading.value = false
                        _error.value = it.message ?: "An unexpected error occurred"
                    }
                }
            }
        }
    }
}