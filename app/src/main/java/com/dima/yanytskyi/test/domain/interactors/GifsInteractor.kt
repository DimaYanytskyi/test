package com.dima.yanytskyi.test.domain.interactors

import com.dima.yanytskyi.test.domain.model.Gif
import com.sobe.favorit.sporter.common.Resources
import kotlinx.coroutines.flow.Flow

interface GifsInteractor {
    fun fetchGifs(q: String, offset: Int) : Flow<Resources<List<Gif>>>
    fun getGifs() : Flow<Resources<List<Gif>>>
    suspend fun insertGifs(list: List<Gif>)
}