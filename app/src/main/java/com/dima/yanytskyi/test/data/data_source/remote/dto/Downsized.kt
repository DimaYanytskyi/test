package com.dima.yanytskyi.test.data.data_source.remote.dto

data class Downsized(
    val height: String,
    val size: String,
    val url: String,
    val width: String
)