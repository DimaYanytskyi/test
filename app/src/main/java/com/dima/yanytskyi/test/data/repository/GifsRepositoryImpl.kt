package com.dima.yanytskyi.test.data.repository

import com.dima.yanytskyi.test.data.data_source.local.GifsDao
import com.dima.yanytskyi.test.data.data_source.remote.GifsApi
import com.dima.yanytskyi.test.data.data_source.remote.dto.GifsDto
import com.dima.yanytskyi.test.domain.model.Gif
import com.dima.yanytskyi.test.domain.repository.GifsRepository
import javax.inject.Inject

class GifsRepositoryImpl @Inject constructor(
    private val gifsApi: GifsApi,
    private val gifsDao: GifsDao
) : GifsRepository {

    override suspend fun fetchGifs(q: String, offset: Int): GifsDto {
        return gifsApi.fetchGifsByQuery(
            q = q,
            offset = offset
        )
    }

    override suspend fun getGifs(): List<Gif> {
        return gifsDao.getGifs()
    }

    override suspend fun insertGifs(list: List<Gif>) {
        gifsDao.insertGifsList(list)
    }

}