package com.dima.yanytskyi.test.data.data_source.remote.dto

data class Preview(
    val height: String,
    val mp4: String,
    val mp4_size: String,
    val width: String
)