package com.dima.yanytskyi.test.data.data_source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dima.yanytskyi.test.domain.model.Gif

@Database(
    entities = [Gif::class],
    version = 1
)
abstract class GifsDatabase : RoomDatabase() {
    abstract val gifsDao : GifsDao
}