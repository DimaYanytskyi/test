package com.dima.yanytskyi.test.presentation.main_screen


sealed class MainScreenEvent {
    object OnNext : MainScreenEvent()
    object OnPrev : MainScreenEvent()
    data class OnSearchInputChanged(val text: String) : MainScreenEvent()
}
